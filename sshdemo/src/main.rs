mod parser;

use anyhow::Error;
use envconfig::Envconfig;
use futures::Future;
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::path::Path;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use thrussh::server::{Auth, Session};
use thrussh::*;
use thrussh_keys::key::KeyPair;
use thrussh_keys::PublicKeyBase64;
use thrussh_keys::*;
use tokio;
use tokio::fs::create_dir_all;
use tokio::task::block_in_place;

#[derive(Envconfig)]
struct Config {
    #[envconfig(from = "CONFIG_DIR", default = "./config")]
    pub config_dir: PathBuf,
    #[envconfig(from = "SSH_PORT", default = "2225")]
    pub port: u16,
}

async fn load_or_create_keypair(
    path_private: impl AsRef<Path>,
    path_public: impl AsRef<Path>,
    passphrase: Option<&str>,
) -> Result<KeyPair, Error> {
    block_in_place(move || match thrussh_keys::load_secret_key(&path_private, passphrase) {
        Ok(v) => {
            eprintln!("--- loaded keypair from {}", path_private.as_ref().to_str().unwrap());
            Ok(v)
        }
        Err(_) => {
            let key = thrussh_keys::key::KeyPair::generate_ed25519().unwrap();
            let f = OpenOptions::new()
                .create(true)
                .truncate(true)
                .write(true)
                .read(false)
                .open(&path_private)?;
            thrussh_keys::encode_pkcs8_pem(&key, f)?;
            eprintln!(
                "--- created ED25519 keypair and wrote it to {}",
                path_private.as_ref().to_str().unwrap()
            );
            let f = OpenOptions::new()
                .create(true)
                .truncate(true)
                .write(true)
                .read(false)
                .open(&path_public)?;
            thrussh_keys::write_public_key_base64(f, &key.clone_public_key())?;
            eprintln!("--- wrote public key to {}", path_public.as_ref().to_str().unwrap());
            Ok(key)
        }
    })
}

#[tokio::main]
async fn main() {
    // envlogger::init();
    let config = Config::init_from_env().unwrap();
    create_dir_all(&config.config_dir).await.unwrap();

    let client_key = thrussh_keys::key::KeyPair::generate_ed25519().unwrap();
    let client_pubkey = Arc::new(client_key.clone_public_key());
    let mut ssh_config = thrussh::server::Config::default();
    ssh_config.connection_timeout = Some(std::time::Duration::from_secs(60));
    ssh_config.auth_rejection_time = std::time::Duration::from_secs(10);
    // config.keys.push(thrussh_keys::key::KeyPair::generate_ed25519().unwrap());
    ssh_config.keys.push(
        load_or_create_keypair(
            config.config_dir.join("test.server.key"),
            config.config_dir.join("test.server.key.pub"),
            None,
        )
        .await
        .unwrap(),
    );
    let ssh_config = Arc::new(ssh_config);
    let sh = Server {
        client_pubkey,
        clients: Arc::new(Mutex::new(HashMap::new())),
        id: 0,
    };
    thrussh::server::run(ssh_config, &format!("0.0.0.0:{}", config.port), sh)
        .await
        .unwrap();
}

#[derive(Clone)]
struct Server {
    client_pubkey: Arc<thrussh_keys::key::PublicKey>,
    clients: Arc<Mutex<HashMap<(usize, ChannelId), thrussh::server::Handle>>>,
    id: usize,
}

impl server::Server for Server {
    type Handler = Self;
    fn new(&mut self, _: Option<std::net::SocketAddr>) -> Self {
        let s = self.clone();
        self.id += 1;
        s
    }
}

impl server::Handler for Server {
    type Error = anyhow::Error;
    type FutureAuth = futures::future::Ready<Result<(Self, server::Auth), anyhow::Error>>;
    type FutureUnit = futures::future::Ready<Result<(Self, Session), anyhow::Error>>;
    type FutureBool = futures::future::Ready<Result<(Self, Session, bool), anyhow::Error>>;

    fn finished_auth(self, auth: Auth) -> Self::FutureAuth {
        println!("finished auth");
        futures::future::ready(Ok((self, auth)))    
    }
    fn finished_bool(self, b: bool, s: Session) -> Self::FutureBool {
        println!("finished bool");
        futures::future::ready(Ok((self, s, b)))
    }
    fn finished(self, s: Session) -> Self::FutureUnit {
        println!("finished");
        futures::future::ready(Ok((self, s)))
    }
    fn channel_open_session(self, channel: ChannelId, session: Session) -> Self::FutureUnit {
        {
            let mut clients = self.clients.lock().unwrap();
            clients.insert((self.id, channel), session.handle());
        }
        println!("channel opened");
        self.finished(session)
    }
    fn auth_publickey(self, _: &str, _: &key::PublicKey) -> Self::FutureAuth {
        println!("auth key success");
        self.finished_auth(server::Auth::Accept)
    }
    fn exec_request(self, channel: ChannelId, data: &[u8], session: Session) -> Self::FutureUnit {
        self.finished(session)
    }

    fn data(self, channel: ChannelId, data: &[u8], mut session: Session) -> Self::FutureUnit {
        {
            let mut clients = self.clients.lock().unwrap();
            for ((id, channel), ref mut s) in clients.iter_mut() {
                if *id != self.id {
                    s.data(*channel, CryptoVec::from_slice(data));
                }
                println!("{:?}", channel);
            }
        }
        println!("{:?}", data);
        session.data(channel, CryptoVec::from_slice(data));
        self.finished(session)
    }
}
