use anyhow;
use anyhow::Error;
use envconfig::Envconfig;
use futures;
use futures::Future;
use std::convert::TryInto;
use std::fs::OpenOptions;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use std::thread::sleep;
use thrussh::server::{Auth, Session};
use thrussh::*;
use thrussh_keys::key::KeyPair;
use thrussh_keys::*;
use tokio;
use tokio::fs::create_dir_all;
use tokio::task::block_in_place;

#[derive(Envconfig)]
struct Config {
    #[envconfig(from = "CONFIG_DIR", default = "./config")]
    pub config_dir: PathBuf,
    #[envconfig(from = "SSH_PORT", default = "2225")]
    pub port: u16,
    #[envconfig(from = "USER", default = "user")]
    pub user: String,
}

async fn load_keypair(path_private: impl AsRef<Path>, passphrase: Option<&str>) -> Result<KeyPair, Error> {
    block_in_place(move || match thrussh_keys::load_secret_key(&path_private, passphrase) {
        Ok(v) => {
            eprintln!("--- loaded keypair from {}", path_private.as_ref().to_str().unwrap());
            Ok(v)
        }
        Err(_) => Err(anyhow::anyhow!(
            "--- unable to load keypair from {}",
            path_private.as_ref().to_str().unwrap()
        )),
    })
}

struct Client {}

impl client::Handler for Client {
    type Error = anyhow::Error;
    type FutureUnit = futures::future::Ready<Result<(Self, client::Session), anyhow::Error>>;
    type FutureBool = futures::future::Ready<Result<(Self, bool), anyhow::Error>>;

    fn finished_bool(self, b: bool) -> Self::FutureBool {
        futures::future::ready(Ok((self, b)))
    }
    fn finished(self, session: client::Session) -> Self::FutureUnit {
        futures::future::ready(Ok((self, session)))
    }
    fn check_server_key(self, server_public_key: &key::PublicKey) -> Self::FutureBool {
        println!("check_server_key: {:?}", server_public_key);
        self.finished_bool(true)
    }
    fn channel_open_confirmation(
        self,
        channel: ChannelId,
        max_packet_size: u32,
        window_size: u32,
        session: client::Session,
    ) -> Self::FutureUnit {
        println!("channel_open_confirmation: {:?}", channel);
        self.finished(session)
    }
    fn channel_success(self, channel: ChannelId, session: client::Session) -> Self::FutureUnit {
        println!("channel_success: {:?}", channel);
        self.finished(session)
    }
    fn channel_open_failure(
        self,
        channel: ChannelId,
        reason: ChannelOpenFailure,
        description: &str,
        language: &str,
        session: client::Session,
    ) -> Self::FutureUnit {
        println!("channel_open_failure: {:?}", channel);
        self.finished(session)
    }
    fn data(self, channel: ChannelId, data: &[u8], session: client::Session) -> Self::FutureUnit {
        println!("data on channel {:?}: {:?}", channel, std::str::from_utf8(data));
        self.finished(session)
    }
}

#[tokio::main]
async fn main() {
    let config = Config::init_from_env().unwrap();
    let ssh_config = thrussh::client::Config::default();
    let ssh_config = Arc::new(ssh_config);
    let sh = Client {};

    create_dir_all(&config.config_dir).await.unwrap();

    // let key = thrussh_keys::key::KeyPair::generate_ed25519().unwrap();
    let key = load_keypair(config.config_dir.join("test.server.key"), None)
        .await
        .unwrap_or_else(|e| {
            eprintln!("{}", e);
            std::process::exit(0);
        });
    let mut agent = thrussh_keys::agent::client::AgentClient::connect_env().await.unwrap();
    agent.add_identity(&key, &[]).await.unwrap();
    let mut session = thrussh::client::connect(ssh_config, format!("localhost:{}", config.port), sh)
        .await
        .unwrap();
    println!("connected");
    if session
        .authenticate_future(config.user, key.clone_public_key(), agent)
        .await
        .1
        .unwrap()
    {
        println!("session authenticated");
        let mut channel = session.channel_open_session().await.unwrap();
        channel.data(&b"Hello, world!"[..]).await.unwrap();
        if let Some(msg) = channel.wait().await {
            println!("{:?}", msg)
        }
    }
}
