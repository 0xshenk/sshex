use ssh2::Session;
use std::io::prelude::*;
use std::net::TcpStream;
use std::path::Path;

fn main() {
    // Connect to the local SSH server
    let tcp = TcpStream::connect("127.0.0.1:2225").unwrap();
    let mut session = Session::new().unwrap();
    session.set_tcp_stream(tcp);
    session.handshake().unwrap();
    session.userauth_pubkey_file("user", None,Path::new("./config/server.key"), None).unwrap();

    let mut channel = session.channel_session().unwrap();
    channel.exec("ls").unwrap();
    let mut s = String::new();
    channel.read_to_string(&mut s).unwrap();
    println!("{}", s);
    channel.wait_close();
    println!("{}", channel.exit_status().unwrap());
}
